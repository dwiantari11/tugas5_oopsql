<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');


$sheep = new Animal("shaun");

echo "Nama Binatang : ". $sheep->name ."<br>"; // "shaun"
echo "Jumlah Kaki : " . $sheep->legs . "<br>"; // 2
echo "Col Blooded : " . $sheep->cold_blooded . "<br> <br>";// false


$kodok = new Frog("buduk");
echo "Nama Binatang : ". $kodok->name ."<br>"; 
echo "Jumlah Kaki : " . $kodok->legs . "<br>";
echo "Col Blooded : " . $kodok->cold_blooded . "<br>";
echo "Jump : ". $kodok->jump()."<br> <br>" ; // "hop hop"

$sungokong = new Ape("kera sakti");
echo "Nama Binatang : ". $sungokong->name ."<br>";
echo "Jumlah Kaki : " . $sungokong->legs . "<br>"; 
echo "Col Blooded : " .  $sungokong->cold_blooded . "<br>";
echo "Yell : ". $sungokong->yell(); // "Auooo"
 

?>