1. Buat Database
CREATE DATABASE myshop;

2. Membuat Tabel didalam Database
->Users
CREATE TABLE users( id integer(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255), email varchar(255), password varchar(255) );

->categories
CREATE TABLE categories( id integer(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255));

->items
CREATE TABLE items( id integer(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255), description varchar(255), price integer, stock integer, category_id integer(8), FOREIGN KEY (category_id) REFERENCES categories(id) );

3. Memasukan Data Pada Tabel
->Users
INSERT INTO users (name,email,password) VALUES ('John Doe','john@doe.com','john123');
INSERT INTO users (name,email,password) VALUES ('Jane Doe','jane@doe.com','jenita123');

->categories
INSERT INTO categories (name) VALUES ('gadget'),('cloth'),('men'),('women'),('branded');

->items
INSERT INTO items (name,description,price,stock,category_id) VALUES ('Sumsang b50','hape keren dari merek sumsang','4000000','100',1), ('Uniklooh','baju keren dari brand ternama','500000','50',2), ('IMHO Watch','jam tangan anak yang jujur banget','2000000','10',1);

4. Mengambil data dari database
a. Mengambil data users
SELECT id, name, email FROM users;

b.Mengambil data items
SELECT * FROM `items` WHERE price > 1000000;
SELECT * FROM `items` WHERE name LIKE 'u%';

c.mengambil data items join dengan kategori
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name FROM items INNER JOIN categories ON items.category_id = categories.id;

5. Mengubah Data Dari Database
UPDATE items SET price = "2500000" WHERE id = 1;
